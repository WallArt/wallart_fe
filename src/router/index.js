import { createRouter, createWebHashHistory } from "vue-router";
import App from "../App.vue";
import LogIn from "../components/LogIn.vue";
import Home from "../components/Home.vue";
import SignUp from "../components/SignUp.vue";
import PrincipalPage from "../components/PrincipalPage.vue";
import ConfigUser from "../components/ConfigUser.vue"
const routes = [
  {
    path: "/",
    name: "root",
    component: App,
  },
  {
    path: "/login",
    name: "LogIn",
    component: LogIn,
  },
  {
    path: "/Home",
    name: "Home",
    component: Home,
  },
  {
    path: "/SignUp",
    name: "SignUp",
    component: SignUp,
  },
  {
    path: "/PrincipalPage",
    name: "PrincipalPage",
    component: PrincipalPage,
  },
  {
    path: "/ConfigUser",
    name: "ConfigUser",
    component: ConfigUser,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
